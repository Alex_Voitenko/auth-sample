import * as R from 'ramda';
import mongoose from 'mongoose';
import { MongoMemoryReplSet } from 'mongodb-memory-server-global-4.4';
import { connectToDB } from '../startup';

const defaultReplOpts = {
  replSet: { storageEngine: 'wiredTiger' },
};

export const connectToTestReplSet = async (opts = {}, mongooseOpts = {}) => {
  try {
    const replSet = new MongoMemoryReplSet(R.mergeDeepRight(defaultReplOpts, opts));
    await replSet.waitUntilRunning();
    const uri = `${await replSet.getUri()}&retryWrites=true&w=majority`;

    await connectToDB(uri, mongooseOpts);

    const close = async () =>
      new Promise((res, rej) => {
        setTimeout(async () => {
          try {
            await mongoose.disconnect();
            await replSet.stop();
            res();
          } catch (error) {
            rej(error);
          }
        }, 500);
      });

    return {
      uri,
      replSet,
      mongoose,
      close,
    };
  } catch (error) {
    console.error('connectToTestReplSet: ', error);
    return {};
  }
};
