import { createTestClient } from 'apollo-server-testing';
import { createApolloServer } from '../apollo-server';

const handleResponse = (res) => {
  if (res && res.errors && res.errors.length) {
    return Promise.reject(res);
  }
  return Promise.resolve(res);
};

export const createApolloClient = ({ context, rejectOnError = true, ...options } = {}) => {
  const apolloServer = createApolloServer({
    context: () => context,
    ...options,
  });

  const testClient = createTestClient(apolloServer);

  if (rejectOnError) {
    return {
      ...testClient,
      mutate: (...args) => testClient.mutate(...args).then(handleResponse),
      query: (...args) => testClient.query(...args).then(handleResponse),
    };
  }

  return testClient;
};
