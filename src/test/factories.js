import faker from 'faker';

export const buildSignUpInput = overrides => ({
  email: faker.internet.email().toLowerCase(),
  password: faker.internet.password(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  ...overrides,
});

export const buildUser = overrides => ({
  email: faker.internet.email().toLowerCase(),
  password: faker.internet.password(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  ...overrides,
});

export const testFactory = {
  buildSignUpInput,
  buildUser,
};
