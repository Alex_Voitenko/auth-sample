export const mockLogError = () => {
  jest.spyOn(console, 'error').mockImplementation(() => null);
};

export const mocks = {
  mockLogError,
};
