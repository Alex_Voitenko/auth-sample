import http from 'http';
import cors from 'cors';
import express from 'express';
import bodyParser from 'body-parser';
import { createApolloServer } from './apollo-server';

export const createHttpServer = ({ port = process.env.PORT } = {}) => {
  const app = express();
  const corsOpts = {};

  app.use(cors(corsOpts));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

  const httpServer = http.createServer(app);

  const apolloServer = createApolloServer();
  apolloServer.applyMiddleware({ app, cors: corsOpts });

  httpServer.stop = () =>
    new Promise((resolve, reject) => {
      httpServer.close(error => error ? reject(error) : resolve(httpServer));
    });

  httpServer.start = () =>
    new Promise((resolve, reject) => {
      httpServer
        .on('error', console.error)
        .listen(port, (error) => {
          if (error) {
            console.error(`Failed to start server: ${error}`);
            reject(error);
          } else {
            resolve({
              httpServer,
              gqlServer: apolloServer,
              app,
              port,
              url: `http://localhost:${port}`,
            });
          }
        });
    });

  return httpServer;
};
