import * as R from 'ramda';

export const combineResolvers = R.reduce(R.mergeDeepRight, {});
