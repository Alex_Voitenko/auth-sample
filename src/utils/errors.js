import { ApolloError } from 'apollo-server-express';

export const ERROR_MESSAGES = {
  INVALID_EMAIL: 'Invalid email',
  INVALID_PASSWORD: 'Invalid password format',
  INVALID_EMAIL_OR_PWD: 'Invalid email or password',
  USER_NOT_FOUND: 'User not found',
  EMAIL_TAKEN: 'User with the same email already exits',
  TOKEN_EXPIRED: 'Token has been expired',
  TOKEN_NOT_FOUND: 'Token not found',
};

export const ERROR_CODES = {
  NOT_FOUND: 'NOT_FOUND',
  EMAIL_TAKEN: 'EMAIL_TAKEN',
  INVALID_USER_INPUT: 'INVALID_USER_INPUT',
  INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
  TOKEN_EXPIRED: 'TOKEN_EXPIRED',
};

export const throwApolloError = (message, code, extensions) => {
  throw new ApolloError(message, code, extensions);
};
