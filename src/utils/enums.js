export const USER_ROLE = {
  ADMIN: 'ADMIN',
  MODERATOR: 'MODERATOR',
};

export const DB_COLLECTION_NAME = {
  USERS: 'users',
  REFRESH_TOKENS: 'refreshTokens',
};
