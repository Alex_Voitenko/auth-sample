import { createHttpServer } from './http-server';
import { startup } from './startup';

const runApp = async () => {
  try {
    if (process.env.NODE_ENV !== 'test') {
      await startup();
    }

    const httpServer = createHttpServer({ port: process.env.PORT || 8080 });
    const { url, gqlServer } = await httpServer.start();

    console.log(`Server ready at ${url}${gqlServer.graphqlPath}`);
  } catch (error) {
    console.log('[Run App Error] ', error);
    process.exit(1);
  }
};

runApp();
