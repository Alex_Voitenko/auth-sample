import mongoose, { Schema } from 'mongoose';
import { DB_COLLECTION_NAME } from '../../utils/enums';

export const RefreshTokenSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: DB_COLLECTION_NAME.USERS,
    required: true,
  },
  refreshToken: {
    type: String,
    required: true,
    index: true,
  },
  expiredAt: {
    type: Date,
    required: true,
    default: () => new Date(),
  },
}, {
  timestamps: true,
});

RefreshTokenSchema.index({ refreshToken: 1 }, { unique: true });

export const RefreshToken = mongoose.model(
  'RefreshToken',
  RefreshTokenSchema,
  DB_COLLECTION_NAME.REFRESH_TOKENS,
);

