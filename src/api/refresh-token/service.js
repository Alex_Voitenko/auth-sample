import { RefreshToken } from './model';
import { ERROR_CODES, ERROR_MESSAGES, throwApolloError } from '../../utils/errors';

export class RefreshTokenService {
  static create = (...args) =>
    RefreshToken.create(...args);

  static findOneByToken = (refreshToken, options) =>
    RefreshToken.findOne({ refreshToken }, options);

  static findOneByTokenAndDelete = async (refreshToken, options) => {
    const refreshTokenDoc = await RefreshToken.findOneAndDelete({ refreshToken }, options);
    if (!refreshTokenDoc) {
      throwApolloError(ERROR_MESSAGES.TOKEN_NOT_FOUND, ERROR_CODES.NOT_FOUND);
    }
    if (refreshTokenDoc.expiredAt < new Date()) {
      throwApolloError(ERROR_MESSAGES.TOKEN_EXPIRED, ERROR_CODES.TOKEN_EXPIRED);
    }
    return refreshTokenDoc;
  };

  static find = (query, options) =>
    RefreshToken.find(query, null, options);

  static deleteMany = (query, options) =>
    RefreshToken.deleteMany(query, options);
}
