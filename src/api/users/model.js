import mongoose from 'mongoose';
import sanitizeHtml from 'sanitize-html';
import { USER_ROLE, DB_COLLECTION_NAME } from '../../utils/enums';
import { ERROR_MESSAGES } from '../../utils/errors';
import { isValidEmail } from '../../utils/validators';

const { Schema } = mongoose;

export const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    index: true,
    unique: true,
    validate: [isValidEmail, ERROR_MESSAGES.INVALID_EMAIL],
  },
  pwdHash: {
    type: String,
  },
  firstName: {
    type: String,
    trim: true,
    set: val => val && sanitizeHtml(val),
  },
  lastName: {
    type: String,
    trim: true,
    set: val => val && sanitizeHtml(val),
  },
  roles: {
    type: [String],
    enum: Object.values(USER_ROLE),
    default: [],
  },
});

export const User = mongoose.model('User', UserSchema, DB_COLLECTION_NAME.USERS);
