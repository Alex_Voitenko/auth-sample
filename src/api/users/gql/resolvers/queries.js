export const userQueries = {
  Query: {
    currentUser: (_, __, { user }) => user,
  },
};
