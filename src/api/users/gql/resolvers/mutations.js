import {
  signUp,
  signIn,
  logout,
  refreshUserTokenPair,
} from '../../utils';

export const userMutations = {
  Mutation: {
    signUp: (_, { input }) => signUp(input),
    signIn: (_, { input }) => signIn(input),
    logout: (_, __, { user }) => logout(user._id),
    refreshUserTokenPair: (_, { refreshToken }) =>
      refreshUserTokenPair(refreshToken),
  },
};
