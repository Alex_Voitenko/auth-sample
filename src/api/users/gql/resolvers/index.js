import { combineResolvers } from '../../../../utils/gql';
import { userQueries } from './queries';
import { userMutations } from './mutations';

export const userResolvers = combineResolvers([
  userQueries,
  userMutations,
]);
