import { userEnums } from './enums';
import { userTypes } from './types';
import { userQueries } from './queries';
import { userMutations } from './mutations';

export const userTypeDefs = [userEnums, userTypes, userQueries, userMutations];
