import { gql } from 'apollo-server-express';

export const userTypes = gql`
  type User {
    _id: ID!
    email: Email!
    firstName: String
    lastName: String
    roles: [UserRole!]
  }

  directive @auth(roles: [UserRole] = []) on FIELD_DEFINITION
`;
