import { gql } from 'apollo-server-express';

export const userMutations = gql`
  input SignUpInput {
    email: Email!
    password: String!
    firstName: String
    lastName: String
  }

  input SignInInput {
    email: Email!
    password: String!
  }
  
  type AuthResponse {
    accessToken: String!
    refreshToken: String!
    user: User!
  }
  
  extend type Mutation {
    signUp(input: SignUpInput!): AuthResponse!
    signIn(input: SignInInput!): AuthResponse!
    logout: Boolean! @auth
    refreshUserTokenPair(refreshToken: String!): AuthResponse!
  }
`;
