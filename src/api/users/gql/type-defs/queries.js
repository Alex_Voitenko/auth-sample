import { gql } from 'apollo-server-express';

export const userQueries = gql`
  extend type Query {
    currentUser: User! @auth
  }
`;
