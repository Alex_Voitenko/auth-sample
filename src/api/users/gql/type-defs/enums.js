import { gql } from 'apollo-server-express';

export const userEnums = gql`
  enum UserRole {
    ADMIN
    MODERATOR
  }
`;
