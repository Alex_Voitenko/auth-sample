import { gql } from 'apollo-server-express';
import MockDate from 'mockdate';
import mongoose from 'mongoose';
import { RefreshTokenService } from '../../refresh-token/service';
import { ERROR_MESSAGES, ERROR_CODES } from '../../../utils/errors';
import { REFRESH_TOKEN_TTL, createUserWithPassword, issueUserTokenPair } from '../utils';
import {
  mocks,
  testFactory,
  connectToTestReplSet,
  createApolloClient,
} from '../../../test';

jest.setTimeout(15000);

let mongo;
beforeAll(async () => {
  mocks.mockLogError();
  mongo = await connectToTestReplSet();
});

afterAll(async () => {
  jest.restoreAllMocks();
  await mongo.close();
});

const REFRESH_TOKEN = gql`
  mutation RefreshUserTokenPair($refreshToken: String!) {
    refreshUserTokenPair(refreshToken: $refreshToken) {
      accessToken
      refreshToken
      user {
        _id
        email
      }
    }
  }
`;

const LOGOUT = gql`
  mutation logout {
    logout
  }
`;

test('User can refresh access token by refresh token', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);
  const { refreshToken } = await issueUserTokenPair(existingUser);

  const { mutate } = createApolloClient();
  const { data: { refreshUserTokenPair } } = await mutate({
    mutation: REFRESH_TOKEN,
    variables: { refreshToken },
  });

  expect(refreshUserTokenPair).toMatchObject({
    accessToken: expect.any(String),
    refreshToken: expect.any(String),
    user: {
      _id: existingUser._id.toString(),
    },
  });

  const oldRefreshTokenDoc = await RefreshTokenService.findOneByToken(refreshToken);
  const newRefreshTokenDoc = await RefreshTokenService
    .findOneByToken(refreshUserTokenPair.refreshToken);

  expect(oldRefreshTokenDoc).toBeNull();
  expect(newRefreshTokenDoc).toMatchObject({
    userId: new mongoose.Types.ObjectId(existingUser._id),
    refreshToken: refreshUserTokenPair.refreshToken,
  });
});

test('User can use refresh token only once', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);
  const { refreshToken } = await issueUserTokenPair(existingUser);

  const { mutate } = createApolloClient({ rejectOnError: false });
  const firstRes = await mutate({
    mutation: REFRESH_TOKEN,
    variables: { refreshToken },
  });
  const secondRes = await mutate({
    mutation: REFRESH_TOKEN,
    variables: { refreshToken },
  });

  expect(firstRes.data.refreshUserTokenPair).toMatchObject({
    accessToken: expect.any(String),
    refreshToken: expect.any(String),
    user: {
      _id: existingUser._id.toString(),
    },
  });

  expect(secondRes.errors).toMatchObject([{
    message: ERROR_MESSAGES.TOKEN_NOT_FOUND,
    extensions: { code: ERROR_CODES.NOT_FOUND },
  }]);
});

test('User cannot refresh access token with invalid refresh token', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);
  const { refreshToken } = await issueUserTokenPair(existingUser);

  const { mutate } = createApolloClient({ rejectOnError: false });
  const { errors } = await mutate({
    mutation: REFRESH_TOKEN,
    variables: { refreshToken: `${refreshToken}_invalid` },
  });

  expect(errors).toMatchObject([{
    message: ERROR_MESSAGES.TOKEN_NOT_FOUND,
    extensions: { code: ERROR_CODES.NOT_FOUND },
  }]);
});

test('User cannot refresh access token with expired refresh token', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);
  const { refreshToken } = await issueUserTokenPair(existingUser);

  const { mutate } = createApolloClient({ rejectOnError: false });
  const now = Date.now();
  MockDate.set(now + REFRESH_TOKEN_TTL + 1000 * 60);

  const { errors } = await mutate({
    mutation: REFRESH_TOKEN,
    variables: { refreshToken },
  });

  expect(errors).toMatchObject([{
    message: ERROR_MESSAGES.TOKEN_EXPIRED,
    extensions: { code: ERROR_CODES.TOKEN_EXPIRED },
  }]);
});

test('Refresh token become invalid on logout', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);
  await issueUserTokenPair(existingUser);

  const { mutate } = createApolloClient({ context: { user: existingUser } });
  const refreshTokensDocBeforeLogout = await RefreshTokenService.find({ userId: existingUser._id });
  const { data: { logout } } = await mutate({ mutation: LOGOUT });
  const refreshTokensDocAfterLogout = await RefreshTokenService.find({ userId: existingUser._id });

  expect(logout).toBeTruthy();
  expect(refreshTokensDocBeforeLogout.length).toBe(1);
  expect(refreshTokensDocAfterLogout.length).toBe(0);
});

test('Multiple refresh token are valid', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);

  const firstTokenPair = await issueUserTokenPair(existingUser);
  const secondTokenPair = await issueUserTokenPair(existingUser);

  const { mutate } = createApolloClient({ context: { user: existingUser } });
  const [firstRefreshToken, secondRefreshToken] = await Promise.all([
    mutate({
      mutation: REFRESH_TOKEN,
      variables: { refreshToken: firstTokenPair.refreshToken },
    }),
    mutate({
      mutation: REFRESH_TOKEN,
      variables: { refreshToken: secondTokenPair.refreshToken },
    }),
  ]);

  const refreshTokenDocs = await RefreshTokenService.find({ userId: existingUser._id });

  expect(firstRefreshToken.data.refreshUserTokenPair).toMatchObject({
    refreshToken: expect.any(String),
    accessToken: expect.any(String),
  });
  expect(secondRefreshToken.data.refreshUserTokenPair).toMatchObject({
    refreshToken: expect.any(String),
    accessToken: expect.any(String),
  });

  expect(refreshTokenDocs.length).toBe(2);
});
