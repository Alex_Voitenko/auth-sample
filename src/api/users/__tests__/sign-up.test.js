import { gql } from 'apollo-server-express';
import { UserService } from '../service';
import { ERROR_MESSAGES, ERROR_CODES } from '../../../utils/errors';
import {
  connectToTestReplSet,
  createApolloClient,
  testFactory,
  mocks,
} from '../../../test';

jest.setTimeout(15000);

let mongo;
beforeAll(async () => {
  mocks.mockLogError();
  mongo = await connectToTestReplSet();
});

afterAll(async () => {
  jest.restoreAllMocks();
  await mongo.close();
});

const SIGN_UP = gql`
  mutation SignUp($input: SignUpInput!) {
    signUp(input: $input) {
      accessToken
      refreshToken
      user {
        _id
        email
      }
    }
  }
`;

test('Should sign up user correctly', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await UserService.findOne({ email: input.email });

  const { mutate } = createApolloClient();
  const { data: { signUp: { accessToken, refreshToken, user } } } = await mutate({
    mutation: SIGN_UP,
    variables: { input },
  });

  const registeredUser = await UserService.findOne({ email: input.email });
  expect(existingUser).toEqual(null);
  expect(accessToken).toEqual(expect.any(String));
  expect(refreshToken).toEqual(expect.any(String));
  expect(registeredUser.toObject()).toMatchObject({
    email: user.email,
    pwdHash: expect.any(String),
    roles: [],
  });
});

test('Should throw error if user with the same email already exists', async () => {
  const input = testFactory.buildSignUpInput();
  await UserService.create({ ...input, pwdHash: 'hsh' });

  const { mutate } = createApolloClient({ rejectOnError: false });
  const { errors } = await mutate({
    mutation: SIGN_UP,
    variables: { input },
  });

  expect(errors).toMatchObject([
    { message: ERROR_MESSAGES.EMAIL_TAKEN, extensions: { code: ERROR_CODES.EMAIL_TAKEN } },
  ]);
});

test('Should throw error on invalid sign up input', async () => {
  const invalidPasswordInput = testFactory.buildSignUpInput({ password: '' });
  const invalidEmailInput = testFactory.buildSignUpInput({ email: 'notEmail' });

  const { mutate } = createApolloClient({ rejectOnError: false });
  const invalidPasswordError = await mutate({
    mutation: SIGN_UP,
    variables: { input: invalidPasswordInput },
  });
  const invalidEmailError = await mutate({
    mutation: SIGN_UP,
    variables: { input: invalidEmailInput },
  });

  expect(invalidPasswordError.errors).toMatchObject([
    {
      message: ERROR_MESSAGES.INVALID_PASSWORD,
      extensions: { code: ERROR_CODES.INVALID_USER_INPUT },
    },
  ]);
  expect(invalidEmailError.errors).toMatchObject([
    { extensions: { code: ERROR_CODES.INTERNAL_SERVER_ERROR } },
  ]);
});
