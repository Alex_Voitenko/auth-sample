import { gql } from 'apollo-server-express';
import mongoose from 'mongoose';
import { ERROR_MESSAGES, ERROR_CODES } from '../../../utils/errors';
import { createUserWithPassword } from '../utils';
import {
  mocks,
  testFactory,
  connectToTestReplSet,
  createApolloClient,
} from '../../../test';

jest.setTimeout(15000);

let mongo;
beforeAll(async () => {
  mocks.mockLogError();
  mongo = await connectToTestReplSet();
});

afterAll(async () => {
  jest.restoreAllMocks();
  await mongo.close();
});

const SIGN_IN = gql`
  mutation SignIn($input: SignInInput!) {
    signIn(input: $input) {
      accessToken
      refreshToken
      user {
        _id
        email
        roles
      }
    }
  }
`;

test('Should sign in correctly', async () => {
  const input = testFactory.buildSignUpInput();
  const existingUser = await createUserWithPassword(input);

  const { mutate } = createApolloClient();
  const { data: { signIn: { accessToken, refreshToken, user } } } = await mutate({
    mutation: SIGN_IN,
    variables: { input: { email: input.email, password: input.password } },
  });

  expect(accessToken).toEqual(expect.any(String));
  expect(refreshToken).toEqual(expect.any(String));
  expect(existingUser.toObject()).toMatchObject({
    _id: new mongoose.Types.ObjectId(user._id),
    email: user.email,
    roles: user.roles,
  });
});

test('Login failed with invalid email', async () => {
  const input = testFactory.buildSignUpInput();
  await createUserWithPassword(input);

  const { mutate } = createApolloClient({ rejectOnError: false });
  const { errors } = await mutate({
    mutation: SIGN_IN,
    variables: { input: { email: `${input.email}m`, password: input.password } },
  });

  expect(errors).toMatchObject([{
    message: ERROR_MESSAGES.INVALID_EMAIL_OR_PWD,
    extensions: { code: ERROR_CODES.INVALID_USER_INPUT },
  }]);
});

test('Login failed with invalid password', async () => {
  const input = testFactory.buildSignUpInput();
  await createUserWithPassword(input);

  const { mutate } = createApolloClient({ rejectOnError: false });
  const { errors } = await mutate({
    mutation: SIGN_IN,
    variables: { input: { email: input.email, password: `${input.password}d` } },
  });

  expect(errors).toMatchObject([{
    message: ERROR_MESSAGES.INVALID_EMAIL_OR_PWD,
    extensions: { code: ERROR_CODES.INVALID_USER_INPUT },
  }]);
});
