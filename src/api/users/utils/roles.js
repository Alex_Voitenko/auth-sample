import { intersection } from 'ramda';

export const isUserInAnyRoles = (roles = [], user) => {
  const userRoles = user?.roles || [];
  return !!intersection(roles, userRoles).length;
};

