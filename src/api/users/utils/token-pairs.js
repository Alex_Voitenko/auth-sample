import jwt from 'jsonwebtoken';
import * as R from 'ramda';
import { v4 as uuidv4 } from 'uuid';
import { addMilliseconds } from 'date-fns';
import { USER_PUBLIC_FIELDS } from './constants';
import { UserService } from '../service';
import { RefreshTokenService } from '../../refresh-token/service';

export const REFRESH_TOKEN_TTL = 1000 * 60 * 60 * 24 * 60; // 60 days

const signJWTUserAccessToken = user =>
  jwt.sign(
    { user: R.pick(USER_PUBLIC_FIELDS, user) },
    process.env.JWT_SECRET,
    { expiresIn: '30min' },
  );

export const verifyAccessToken = token =>
  new Promise((res, rej) =>
    jwt.verify(
      token,
      process.env.JWT_SECRET,
      (err, data) => err ? rej(err) : res(data?.user),
    ));

export const invalidateUserRefreshToken = async (oldRefreshToken) => {
  const invalidatedRefreshTokenDoc = await RefreshTokenService
    .findOneByTokenAndDelete(oldRefreshToken);
  const user = await UserService.findOneByIdOrReject(invalidatedRefreshTokenDoc.userId);
  return { user, invalidatedRefreshTokenDoc };
};

export const issueUserTokenPair = async (user) => {
  const refreshToken = uuidv4();
  const refreshTokenDoc = await RefreshTokenService.create({
    refreshToken,
    userId: user._id,
    expiredAt: addMilliseconds(Date.now(), REFRESH_TOKEN_TTL),
  });

  return {
    user,
    refreshTokenDoc,
    refreshToken,
    accessToken: signJWTUserAccessToken(user),
  };
};

export const refreshUserTokenPair = async (oldRefreshToken) => {
  const { user } = await invalidateUserRefreshToken(oldRefreshToken);
  return issueUserTokenPair(user);
};
