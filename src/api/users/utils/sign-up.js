import bcrypt from 'bcryptjs';
import { UserService } from '../service';
import { issueUserTokenPair } from './token-pairs';
import { ERROR_CODES, ERROR_MESSAGES, throwApolloError } from '../../../utils/errors';

const SALT_ROUNDS = 12;

export const createUserWithPassword = async ({ password, ...signUpInput }) => {
  if (password?.length < 5) {
    throwApolloError(ERROR_MESSAGES.INVALID_PASSWORD, ERROR_CODES.INVALID_USER_INPUT);
  }
  const pwdHash = await bcrypt.hash(password, SALT_ROUNDS);
  return UserService.create({ ...signUpInput, pwdHash });
};

export const signUp = async (signUpInput) => {
  const user = await createUserWithPassword(signUpInput);
  return issueUserTokenPair(user);
};
