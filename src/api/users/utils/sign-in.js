import bcrypt from 'bcryptjs';
import { UserService } from '../service';
import { issueUserTokenPair } from './token-pairs';
import { ERROR_MESSAGES, ERROR_CODES, throwApolloError } from '../../../utils/errors';

export const signIn = async ({ email, password }) => {
  const user = await UserService.findOneByEmail(email, {});

  if (!user || !bcrypt.compareSync(password, user?.pwdHash)) {
    throwApolloError(ERROR_MESSAGES.INVALID_EMAIL_OR_PWD, ERROR_CODES.INVALID_USER_INPUT);
  }

  return issueUserTokenPair(user);
};
