import { RefreshTokenService } from '../../refresh-token/service';

export const logout = async (userId) => {
  await RefreshTokenService.deleteMany({ userId });
  return true;
};
