export const USER_PUBLIC_FIELDS = [
  '_id',
  'email',
  'firstName',
  'lastName',
  'roles',
];
