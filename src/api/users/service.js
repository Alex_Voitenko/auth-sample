import { User } from './model';
import { ERROR_MESSAGES, ERROR_CODES, throwApolloError } from '../../utils/errors';

export class UserService {
  static create = async (input, options) => {
    try {
      return await User.create(input, options);
    } catch (error) {
      if (error.code === 11000) {
        throwApolloError(ERROR_MESSAGES.EMAIL_TAKEN, ERROR_CODES.EMAIL_TAKEN);
      }
      throw error;
    }
  }

  static findOne = (query, options) =>
    User.findOne(query, null, options);

  static findOneByIdOrReject = async (id, options) => {
    const user = await User.findById(id, null, options);
    if (!user) {
      throwApolloError(ERROR_MESSAGES.USER_NOT_FOUND, ERROR_CODES.NOT_FOUND);
    }
    return user;
  }

  static findOneByEmail = async (email, options) =>
    User.findOne({ email: email?.trim()?.toLowerCase() }, null, options);
}
