import { ApolloServer } from 'apollo-server-express';
import { typeDefs } from './type-defs';
import { resolvers } from './resolvers';
import { AuthDirective } from './directives';
import { verifyAccessToken } from '../api/users/utils/token-pairs';

const getAccessTokenByReq = (req) => {
  const authorization = req?.headers?.authorization?.trim() || '';
  return authorization.trim().replace(/^Bearer\s+/, '');
};

const getAuthUserByReq = async (req) => {
  const accessToken = getAccessTokenByReq(req);
  if (!accessToken) {
    return null;
  }
  try {
    return await verifyAccessToken(accessToken);
  } catch (error) {
    return null;
  }
};

export const createApolloServer = (opts) => {
  const defaultOptions = {
    typeDefs,
    resolvers,
    schemaDirectives: {
      auth: AuthDirective,
    },
    context: async ({ req, res, connection }) => {
      if (connection) {
        return connection.context;
      }

      const user = await getAuthUserByReq(req);
      return {
        req,
        res,
        user,
      };
    },
    formatError: (error) => {
      console.error('[ApolloError] ', error);
      return error;
    },
  };

  return new ApolloServer({ ...defaultOptions, ...opts });
};

