import { gql } from 'apollo-server-express';
import { userTypeDefs } from '../api/users/gql/type-defs';

export const rootTypeDefs = gql`
  type Query {
    version: String!
  }
  
  type Mutation {
    version: String!
  }
  
  scalar DateTime
  scalar Email
  
  schema {
    query: Query
    mutation: Mutation
  }
`;

export const typeDefs = [
  rootTypeDefs,
  userTypeDefs,
].flat(2);
