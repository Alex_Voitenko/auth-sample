import { defaultFieldResolver } from 'graphql';
import { SchemaDirectiveVisitor, AuthenticationError, ForbiddenError } from 'apollo-server-express';
import { isUserInAnyRoles } from '../api/users/utils';

export class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    this.wrapField(field);
  }

  wrapField(field) {
    const { roles = [] } = this.args;
    const { resolve = defaultFieldResolver } = field;

    // eslint-disable-next-line
    field.resolve = function resolver(...args) {
      const { user } = args[2] || {};

      if (!user) {
        throw new AuthenticationError('Not Authorized');
      }

      if (roles.length && !isUserInAnyRoles(roles, user)) {
        throw new ForbiddenError('Permission denied');
      }

      return resolve.apply(this, args);
    };
  }
}
