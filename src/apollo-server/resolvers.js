import { DateTime, EmailAddress as Email } from '@okgrow/graphql-scalars';
import { combineResolvers } from '../utils/gql';
import { userResolvers } from '../api/users/gql/resolvers';

export const rootResolvers = {
  DateTime,
  Email,
  Query: {
    version: () => '1.0.0',
  },
  Mutation: {
    version: () => '1.0.0',
  },
};

export const resolvers = combineResolvers([
  rootResolvers,
  userResolvers,
]);
