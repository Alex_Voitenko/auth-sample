import mongoose from 'mongoose';

export const connectToDB = (
  uri = process.env.MONGO_URI,
  options = {},
) =>
  mongoose.connect(
    uri,
    {
      poolSize: 10,
      serverSelectionTimeoutMS: 20000,
      socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      ...options,
    },
  );

mongoose.connection.on('error', (error) => {
  console.error(`[Mongoose Connection] ${error}`);
});

export const startup = async () => {
  await connectToDB();
};
