const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env.test') });

process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.TZ = 'UTC';

process.on('unhandledRejection', (err) => {
  throw err;
});

module.exports = {
  roots: [
    '<rootDir>/src',
  ],
  maxConcurrency: 4,
  testEnvironment: 'node',
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/dist/',
    '/lib/',
  ],
  watchPlugins: [
    'jest-watch-select-projects',
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
  ],
  collectCoverageFrom: [
    '**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**',
    '!**/test/**',
  ],
  setupFilesAfterEnv: [
    '<rootDir>/src/test/setup-tests.js',
  ],
  displayName: {
    name: 'SERVER',
    color: 'blue',
  },
  transform: {
    '\\.js$': [require.resolve('babel-jest'), { rootMode: 'upward' }],
  },
};
