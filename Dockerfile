FROM node:12.18.3

ENV app_path /usr/src/auth/
RUN mkdir -p ${app_path}
WORKDIR ${app_path}

COPY yarn.lock package.json babel.config.js src ./
COPY src ./src

# Ignore to install an optional mongodb-memory-server because of incompatibilities
# see: https://github.com/nodkz/mongodb-memory-server#known-incompatibilities
RUN yarn install --pure-lockfile --non-interactive --ignore-optional
RUN yarn build

EXPOSE 8080

CMD yarn serve
