module.exports = (api) => {
  api.cache(true);

  return {
    plugins: [
      '@babel/plugin-proposal-optional-chaining',
      ['@babel/plugin-proposal-class-properties', { loose: true }],
    ],
    presets: [
      [
        '@babel/env',
        {
          targets: {
            node: '12.18.3',
          },
        },
      ],
    ],
    env: {
      build: {
        ignore: [
          '**/*.test.tsx',
          '**/*.test.ts',
          '**/*.story.tsx',
          '**/*.test.jsx',
          '**/*.test.js',
          '**/*.story.jsx',
          '__snapshots__',
          '__tests__',
          '__stories__',
        ],
      },
    },
    ignore: ['node_modules'],
  };
};
